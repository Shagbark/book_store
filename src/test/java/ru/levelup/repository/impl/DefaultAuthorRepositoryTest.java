package ru.levelup.repository.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import ru.levelup.domain.Author;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

// @RunWith(MockitoRunner.class) - junit 4
// @ExtendWith(MockitoExtension.class) - junit 5
public class DefaultAuthorRepositoryTest {

//    private SessionFactory factory = new SessionFactoryImpl();
    @Mock
    private SessionFactory sessionFactory;
    @Mock
    private Session session;
    @Mock
    private Transaction transaction;

    @InjectMocks
    private DefaultAuthorRepository authorRepository;

    @BeforeEach
    public void createAuthorRepositoryObject() {
        // this.authorRepository = new DefaultAuthorRepository(new SessionFactoryStub());
        // sessionFactory = Mockito.mock(SessionFactory.class);
        // session = Mockito.mock(Session.class);
        // transaction = Mockito.mock(Transaction.class);

        MockitoAnnotations.initMocks(this);
        Mockito.when(sessionFactory.openSession()).thenReturn(session);
        Mockito.when(session.beginTransaction()).thenReturn(transaction);

        // authorRepository = new DefaultAuthorRepository(sessionFactory);
    }

    @Test
    public void testCreateAuthor_validParameters_createNewAuthor() {
        // given:
        String name = "name";
        String lastName = "lastName";

        // when:
        Author result = authorRepository.createAuthor(name, lastName);

        // then:
        Assertions.assertEquals(name, result.getName());
        Assertions.assertEquals(lastName, result.getLastName());

        // Mockito.verify
        Mockito.verify(session).persist(result);
        Mockito.verify(transaction).commit();
        Mockito.verify(session).close();
    }

    @Test
    public void testCreateAuthor_nameIsNull_throwException() {
        //   void execute() throws Throwable;
        Assertions.assertThrows(NullPointerException.class, () ->
                authorRepository.createAuthor(null, "last name"));
    }

    @Test
    public void testCreateAuthor_lastNameIsNull_throwException() {
        //   void execute() throws Throwable;
        Assertions.assertThrows(NullPointerException.class, () ->
                authorRepository.createAuthor("name", null));
    }

    @Test
    public void testFindAll_recordsExist_returnAllAuthors() {
        // s = sessionFactory.openSession();
        // run(session -> session.createQuery("from Author", Author.class).getResultList());
        // s.close();
        List<Author> expectedResult = Arrays.asList(
                new Author("name1", "lastname1"),
                new Author("name2", "lastname2")
        );

        Query query = Mockito.mock(Query.class);
        Mockito.when(session.createQuery("from Author", Author.class)).thenReturn(query);
        Mockito.when(query.getResultList()).thenReturn(expectedResult);

        // when:
        Collection<Author> result = authorRepository.findAll();

        // then:
        Assertions.assertEquals(expectedResult.size(), result.size());
        Mockito.verify(session).close();
    }

}
