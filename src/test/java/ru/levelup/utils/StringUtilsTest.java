package ru.levelup.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StringUtilsTest {

    // @BeforeAll       (BeforeClass)   static
    // @BeforeEach      (Before)

    // @AfterAll        (AfterClass)    static
    // @AfterEach       (After)

    // testNonBlank_nullString_returnFalse
    // testNonBlank_whenNullPassed_thenReturnFalse

    // null -> false
    // "     " -> false
    // " 435 234" -> true
    // "542 2353 " -> true
    // "" -> false
    // "534834" -> true
    @Test
    public void testNonBlank_nullString_returnFalse() {
        boolean result = StringUtils.notBlank(null);
        Assertions.assertFalse(result);
    }

    @Test
    public void testNonBlank_notEmptyString_returnTrue() {
        Assertions.assertTrue(StringUtils.notBlank("    634 64534"));
        Assertions.assertTrue(StringUtils.notBlank("634 45645    "));
        Assertions.assertTrue(StringUtils.notBlank("45612"));
    }

    @Test
    public void testNonBlank_emptyString_returnFalse() {
        boolean result = StringUtils.notBlank("");
        Assertions.assertFalse(result);
    }

}
