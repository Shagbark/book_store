package it.ru.levelup.utils;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import ru.levelup.domain.Author;
import ru.levelup.domain.AuthorBook;
import ru.levelup.domain.Book;

import java.util.Properties;

public class TestHibernateUtils {

    private static SessionFactory factory;

    //         <property name="hibernate.connection.driver_class">org.postgresql.Driver</property>
    //        <property name="hibernate.connection.url">jdbc:postgresql://127.0.0.1:5432/books</property>
    //        <property name="hibernate.connection.username">postgres</property>
    //        <property name="hibernate.connection.password">root</property>
    //        <property name="hibernate.dialect">org.hibernate.dialect.PostgreSQL95Dialect</property>
    //        <!-- validate -->
    //        <!-- update -->
    //        <!-- create -->
    //        <!-- create-drop -->
    //        <!-- DB: 1, 2, 3, 4, 5, 6  HB: 1, 2, 3, 4, 6-->
    //        <property name="hibernate.hbm2ddl.auto">validate</property>
    //        <property name="show_sql">true</property>
    //        <property name="format_sql">true</property>
    static {
        Properties properties = new Properties();
        properties.setProperty("hibernate.connection.driver_class", "org.h2.Driver");
        properties.setProperty("hibernate.connection.url", "jdbc:h2:mem:bookstore;INIT=create schema if not exists bookstore");
        properties.setProperty("hibernate.connection.username", "sa");
        properties.setProperty("hibernate.connection.password", "");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
        properties.setProperty("show_sql", "true");
        properties.setProperty("format_sql", "true");

        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .applySettings(properties)
                .build();

        factory = new Configuration()
                .addAnnotatedClass(Book.class)
                .addAnnotatedClass(Author.class)
                .addAnnotatedClass(AuthorBook.class)
                // .configure()
                .buildSessionFactory(registry);
    }

    public static SessionFactory getFactory() {
        return factory;
    }

}
