package it.ru.levelup.repository.impl;

import it.ru.levelup.utils.TestHibernateUtils;
import org.hibernate.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.levelup.domain.Author;
import ru.levelup.repository.impl.DefaultAuthorRepository;

public class DefaultAuthorRepositoryIT {

    private static DefaultAuthorRepository defaultAuthorRepository;

    @BeforeAll
    public static void setupRepository() {
        defaultAuthorRepository = new DefaultAuthorRepository(TestHibernateUtils.getFactory());
    }

    @Test
    public void testCreateAuthor_validParams_createAuthor() {
        String name = "name";
        String lastName = "last_name";

        Author author = defaultAuthorRepository.createAuthor(name, lastName);
        Assertions.assertNotNull(author.getId());

        Session session = TestHibernateUtils.getFactory().openSession();
        Author fromDB = session.get(Author.class, author.getId());
        session.close();

        Assertions.assertEquals(fromDB.getName(), author.getName());
        Assertions.assertEquals(fromDB.getLastName(), author.getLastName());
    }

}
