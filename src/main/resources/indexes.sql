analyze author;
select * from author;
set enable_seqscan = off;
explain analyze select * from author where id = 2;
explain analyze select * from author where name = 'James';

create index author_id_idx2 on author (id);
create index author_name_idx on author (name);

explain analyze insert into author(name, last_name, age) values ('Tod', 'Tod', 33);