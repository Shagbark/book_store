-- publisher
-- book
-- author

-- book_author
    -- book_id
    -- author_id
    -- chapter_count
create table book
(
    id serial primary key,
    name varchar not null unique,
    year integer not null
);

create table author
(
    id serial primary key,
    name varchar not null,
    last_name varchar not null
);

create table author_book
(
    book_id integer not null,
    author_id integer not null,
    chapter_count integer not null,
    constraint author_book_pkey primary key (book_id, author_id),
    foreign key (book_id) references book(id),
    foreign key (author_id) references author(id)
);