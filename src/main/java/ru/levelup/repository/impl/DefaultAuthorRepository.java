package ru.levelup.repository.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import ru.levelup.domain.Author;
import ru.levelup.repository.AuthorRepository;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Function;

public class DefaultAuthorRepository extends AbstractRepository implements AuthorRepository {

    public DefaultAuthorRepository(SessionFactory factory) {
        super(factory);
    }

    @Override
    public Author createAuthor(String name, String lastName) {
        Objects.requireNonNull(name, "The name must be non null");
        Objects.requireNonNull(lastName, "The last name must be non null");

        return runInTransaction(new Function<Session, Author>() {
            @Override
            public Author apply(Session session) {
                Author author = new Author(null, lastName);
                session.persist(author);
                return author;
            }
        });
//        return runInTransaction(new CreateAuthorFunction(name, lastName));
//        return runInTransaction(session -> {
//            Author author = new Author(name, lastName);
//            session.persist(author);
//
//            return author;
//        });
    }

    @SuppressWarnings("InnerClassMayBeStatic")
    private class CreateAuthorFunction implements Function<Session, Author> {
        private String name;
        private String lastName;
        CreateAuthorFunction(String name, String lastName) {
            this.name = name;
            this.lastName = lastName;
        }
        @Override
        public Author apply(Session session) {
            Author author = new Author(name, lastName);
            session.persist(author);
            return author;
        }
    }

    @Override
    public void removeAuthor(Integer authorId) {
        runInTransaction(session -> {
            session.createQuery("delete from Author where id = :authorId")
                    .setParameter("authorId", authorId)
                    .executeUpdate();

            // Author author = session.load(Author.class, authorId);
            // if (author != null)
            //      session.delete(author)
            // Optional.ofNullable(session.load(Author.class, authorId))
            //         .ifPresent(session::delete);
        });
    }

    @Override
    public Collection<Author> findAll() {
        return run(session -> session.createQuery("from Author", Author.class).getResultList());
    }

    @Override
    public Collection<Author> findByName(String name) {
        return run(session -> session.createQuery("from Author where name = :name", Author.class)
                .setParameter("name", name)
                .getResultList()
        );
    }

}
