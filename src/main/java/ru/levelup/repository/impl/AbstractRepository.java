package ru.levelup.repository.impl;

import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.function.Consumer;
import java.util.function.Function;

@AllArgsConstructor
public abstract class AbstractRepository {

    protected SessionFactory factory;

    /**
     * Run code in transaction. This method used, when need to return
     * result value after method completion.
     *
     * @param function function to run in transaction
     * @param <T> result entity (usually) or some other return value
     * @return function result
     */
    protected <T> T runInTransaction(Function<Session, T> function) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        T result = null;
        try {
            result = function.apply(session);
            transaction.commit();
        } catch (Exception exc) {
            System.out.println("Произошла ошибка в рамка транзакции");
            transaction.rollback();
        }

        session.close();

        return result;
    }

    /**
     * Run code in transaction. This method used, when we don't need to
     * return value.
     *
     * @param consumer the code, which need to invoke in transaction
     */
    protected void runInTransaction(Consumer<Session> consumer) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        consumer.accept(session);

        transaction.commit();
        session.close();
    }

    protected <T> T run(Function<Session, T> function) {
        try (Session session = factory.openSession()) {
            return function.apply(session);
        }
    }

}
