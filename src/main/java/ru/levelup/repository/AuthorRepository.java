package ru.levelup.repository;

import ru.levelup.domain.Author;

import java.util.Collection;

public interface AuthorRepository {

    Author createAuthor(String name, String lastName);

    Collection<Author> findAll();

    Collection<Author> findByName(String name);

    void removeAuthor(Integer authorId);

}
