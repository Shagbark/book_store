package ru.levelup.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class AuthorBookId implements Serializable {

    private Integer bookId;         // book_id
    private Integer authorId;       // author_id

}
