package ru.levelup.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@NoArgsConstructor
@Table(name = "author_book")
public class AuthorBook implements Serializable {

    @EmbeddedId
    private AuthorBookId authorBookId;

    @ManyToOne
    @MapsId("authorId") // author_id
    private Author author;

    @ManyToOne
    @MapsId("bookId")   // book_id
    private Book book;

    @Column(name = "chapter_count")
    private Integer chapterCount;

    public AuthorBook(Author author, Book book, Integer chapterCount) {
        this.authorBookId = new AuthorBookId(book.getId(), author.getId());
        this.author = author;
        this.book = book;
        this.chapterCount = chapterCount;
    }

}
