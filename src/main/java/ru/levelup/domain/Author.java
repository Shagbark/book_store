package ru.levelup.domain;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@Table(name = "author")
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @Column(name = "last_name")
    private String lastName;

    // @ManyToMany(mappedBy = "authors")
    @OneToMany(mappedBy = "author", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<AuthorBook> books;

    public Author(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
        this.books = new ArrayList<>();
    }

}
