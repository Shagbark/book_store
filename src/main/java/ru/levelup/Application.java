package ru.levelup;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.levelup.domain.Author;
import ru.levelup.domain.AuthorBook;
import ru.levelup.domain.Book;
import ru.levelup.hibernate.HibernateUtils;

public class Application {

    public static void main(String[] args) {
        SessionFactory factory = HibernateUtils.getFactory();
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        Book book = new Book();
        book.setName("Book #2");
        book.setYear(2019);

        session.persist(book);

        Author author = new Author();
        author.setName("AuthName");
        author.setLastName("ALastName");
        session.persist(author);

        Author author2 = new Author();
        author2.setName("Author2");
        author2.setLastName("AuthorLastName2");
        session.persist(author2);

        // book.setAuthors(Arrays.asList(author, author2));
        AuthorBook ab1 = new AuthorBook(author, book, 12);
        AuthorBook ab2 = new AuthorBook(author2, book, 4);
        session.persist(ab1);
        session.persist(ab2);

        transaction.commit();
        session.close();
        factory.close();
    }

}
