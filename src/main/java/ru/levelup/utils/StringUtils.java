package ru.levelup.utils;

public final class StringUtils {

    // "    3 436 ".trim() -> "3 436"
    public static boolean notBlank(String value) {
        return value != null && !value.trim().isEmpty();
    }

}
